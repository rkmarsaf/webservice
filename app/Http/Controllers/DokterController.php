<?php

namespace App\Http\Controllers;

use App\Dokter;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DokterController extends Controller
{
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }
    
    public function index(){

        $dokter = Dokter::all();

        return response()->json( array(
                'status' => true,
                'dokter' => $dokter
            )
        );
    }

    public function dokauth(){

        $dokter = Dokter::all();

        return response()->json( array(
                'status' => true,
                'dokter' => $dokter
            )
        );
    }

    public function create(Request $request)
    {   
        $dokter = Dokter::create($request->all());
        
        if(!$dokter){
            return response()->json( array(
                'status' => false,
                'message' => 'Data gagal di tambahkan',
            ));
        }else{
            return response()->json( array(
                'status' => true,
                'message' => 'Data berhasil di tambahkan',
                'dokter' => $dokter
            ));
        }
    }

    public function update(Request $request, $id)
    {

        $dokter = $request->validate([
            'kd_dokter' => '',
            'nama_dokter' => '',
            'jenis_kelamin' => '',
            'no_telp' => '',
            'alamat' => ''
        ]);

        $response = Dokter::find($id);
        $response->update($dokter);
        
        
        if(!$response){
            return response()->json( array(
                'status' => 'OK',
                'message' => 'Data tidak ditemukan',
            ));
        }else{
            return response()->json( array(
                'status'  => 'OK',
                'message' => 'Data berhasil di update',
                'dokter'  => $response
            ));
        }
    }

    public function destroy($id)
    {
        $dokter = Dokter::find($id);
        $dokter->delete();
        
        if(!$dokter){
            return response()->json( array(
                'status' => 'OK',
                'message' => 'Data tidak ditemukan'
            ));
        }else{
            return response()->json( array(
                'status' => 'OK',
                'message' => 'Data berhasil di hapus'
            ));
        }
    }
}
