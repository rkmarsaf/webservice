<?php

namespace App\Http\Controllers;

use App\Pasien;
use Illuminate\Http\Request;
use JWTAuth;

class PasienController extends Controller
{
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }
    
    public function index(){

        $pasien = Pasien::all();
    
        return response()->json( array(
                'status' => 'OK',
                'pasien' => $pasien
            )
        );
    }

    public function create(Request $request)
    {   
        $pasien = Pasien::create($request->all());

        if(!$pasien){
            return response()->json( array(
                'status' => 'OK',
                'message' => 'Data gagal di tambahkan',
            ));
        }else{
            return response()->json( array(
                'status' => 'OK',
                'message' => 'Data berhasil di tambahkan',
                'pasien' => $pasien
            ));
        }
    }

    public function update(Request $request, $id)
    {

        $pasien = $request->validate([
            'nama_pasien' => '',
            'nmr' => '',
            'tgl_lahir' => '',
            'jenis_kelamin' => '',
            'alamat' => ''
        ]);

        $response = Pasien::find($id);
                 
        if($response){
            $response->update($pasien);
            return response()->json( array(
                'status' => true,
                'message' => 'Data berhasil di update',
                'pasien' => $response
            ));
            
        }else{   
            return response()->json( array(
                'status' => false,
                'message' => 'Data tidak ditemukan',
            ));
        }
    }

    public function destroy($id)
    {
        $pasien = Pasien::find($id);
        
        if($pasien){
            $pasien->delete();
            return response()->json( array(
                'status' => true,
                'message' => 'Data berhasil di hapus'
            ));
        }else{
            return response()->json( array(
                'status' => false,
                'message' => 'Data tidak ditemukan'
            ));
        }
    }
}
