<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController extends Controller
{
    public $loginAfterSignUp = true;

    public function login(Request $request){
        $credentials = $request->only("email", "password");
        $token = null;

        if(!$token = JWTAuth::attempt($credentials)){
            return response()->json([
                "status"  =>false,
                "message" => "Unauthorized"
            ]);
        }

        return response()->json([
            "status" => true,
            "token" => $token
        ]);
    }

    public function register(Request $request){
        $this->validate($request, [
            "name"     => "required|string",
            "email"    => "required|email|unique:users",
            "password" => "required|string|min:6"
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if($this->loginAfterSignUp){
            return $this->login($request);
        }

        return response()->json([
            "status" => true,
            "user"   => $user
        ]);
    }

    public function logout(Request $request){
        $this->validate($request, [
            "token" => "required"
        ]);

        try{
            JWTAuth::invalidate($request->token);
            return response()->json([
                "status" =>true,
                "message" => "User Logged out successfully"
            ]);
        }catch(JWTException $e){
            return response()->json([
                "status" =>false,
                "message" => "Ops, the user can not be logged out"
            ]);
        }
    }

    public function user(){
        try{
            if(!$user = JWTAuth::parseToken()->Authenticate()){
                return response()->json(['user_not_found'], 400);
            }
        }catch(TokenExpiredException $e){
            return response()->json(['token_expired'], $e-getStatusCode());
        }catch(TokenInvalidException $e){
            return response()->json(['token_invalid'], $e-getStatusCode());
        }catch(JWTException $e){
            return response()->json(['token_absent'], $e-getStatusCode());
        }

        return response()->json([
            'status' => true,
            'user'   => $user
        ]);
    }
}
