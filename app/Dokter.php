<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Dokter extends Model
{

    protected $table = 'dokter';
    protected $guarded = [];
    public $timestamps = false;
    public $primaryKey = 'id_dokter'; 
}
