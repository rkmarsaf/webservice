<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DokterController;
use App\Http\Controllers\PasienController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::group(["middleware"=>"auth.jwt"], function(){
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/user', [AuthController::class, 'user']);

    //Pasien
    Route::get('/pasien', [PasienController::class, 'index']);
    Route::post('/pasien/create', [PasienController::class, 'create']);
    Route::put('/pasien/update/{id}', [PasienController::class, 'update']);
    Route::delete('pasien/delete/{id}', [PasienController::class, 'destroy']);

    //Dokter
    Route::get('/dokter', [DokterController::class, 'index']);
    Route::post('/dokter/create', [DokterController::class, 'create']);
    Route::put('/dokter/update/{id}', [DokterController::class, 'update']);
    Route::delete('/dokter/delete/{id}', [DokterController::class, 'destroy']);
});