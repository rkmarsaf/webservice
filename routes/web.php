<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DokterController;
use App\Http\Controllers\PasienController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// //Pasien
// Route::get('/pasien', [PasienController::class, 'index']);
// Route::post('/pasien/create', [PasienController::class, 'create']);
// Route::put('/pasien/update/{id}', [PasienController::class, 'update']);
// Route::delete('pasien/delete/{id}', [PasienController::class, 'destroy']);

// //Dokter
// Route::get('/dokter', [DokterController::class, 'index']);
// Route::post('/dokter/create', [DokterController::class, 'create']);
// Route::put('/dokter/update/{id}', [DokterController::class, 'update']);
// Route::delete('/dokter/delete/{id}', [DokterController::class, 'destroy']);