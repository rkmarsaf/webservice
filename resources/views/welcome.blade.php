@foreach ($dokter as $d)
    <?php
        $item[] = array(
            'id_dokter' => $d->id_dokter,
            'kd_dokter' => $d->kd_dokter,
            'nama_dokter' => $d->nama_dokter,
            'jenis_kelamin' => $d->jenis_kelamin,
            'no_telp' => $d->no_telp,
            'alamat' => $d->alamat
        );
    ?>
@endforeach

<?php
$response = array(
    'status' => 'OK',
    'dokter' => $item
);

echo json_encode($response);
?>

